# RmlUi - The HTML/CSS User Interface Library Evolved

**These are offical standalone samples of the 1970s classic Space Invaders using the [RmlUi](https://github.com/mikke89/RmlUi) interface, that uses [Conan.io](https://conan.io) package manager.**

---

RmlUi is the C++ user interface package based on the HTML and CSS standards, designed as a complete solution for any project's interface needs. It is a fork of the [libRocket](https://github.com/libRocket/libRocket) project, introducing new features, bug fixes, and performance improvements. 

RmlUi uses the time-tested open standards XHTML1 and CSS2 while borrowing features from HTML5 and CSS3, and extends them with features suited towards real-time applications. Because of this, you don't have to learn a whole new proprietary technology like other libraries in this space. Please have a look at the supported [RCSS properties](https://mikke89.github.io/RmlUiDoc/pages/rcss/property_index.html) and [RML elements](https://mikke89.github.io/RmlUiDoc/pages/rml/element_index.html).

Documentation is located at https://mikke89.github.io/RmlUiDoc/

## Dependencies

- [Conan.io](https://conan.io) package manager.
- [FreeType](https://www.freetype.org/).
- [Lua](https://www.lua.org/)
- The standard library.

In addition, a C++14 compatible compiler is required.

## Build

```bash
$ pip3 install conan
$ cmake -S. -Bbuild
$ cmake --build build/ -j4
```

## Run

```bash
$ ./build/bin/invaders
$ ./build/bin/luainvaders
```
