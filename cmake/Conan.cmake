macro(run_conan)
  # Download automatically, you can also just copy the conan.cmake file
  if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
    file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.15/conan.cmake" "${CMAKE_BINARY_DIR}/conan.cmake")
  endif()

  include(${CMAKE_BINARY_DIR}/conan.cmake)

  conan_cmake_run(
    BASIC_SETUP
    BUILD
      missing
    CMAKE_TARGETS
    REQUIRES
      glfw/3.3.2
      glew/2.1.0
      RmlUi/3.3@b110011/testing
    GENERATORS
      cmake_find_package
    OPTIONS
      RmlUi:with_default_font_interface=True
      RmlUi:with_lua_bindings=True
    SETTINGS
      compiler.cppstd=14
)
endmacro()
